import serial
import serial.tools.list_ports as stools_ports
import json
import queue
from multiprocessing import Process, Queue
import time
import threading
import sys,os

#matplotlib imports
import matplotlib.pyplot as plt
import matplotlib.animation as animation
from matplotlib import style

#pyqtgraph imports
import glob
import binascii
import PyQt5
import pyqtgraph as pg
from pyqtgraph.Qt import QtCore, QtGui
#from PyQt5.QtWidgets import QtGui, QtCore

class SerialDataMonitorThread(threading.Thread):
    def __init__(self, timeout, serialport = None, filename = 'datafile.csv', queuesize = 100):
        super(SerialDataMonitorThread, self).__init__()
        self._queuesize = queuesize
        self._q = queue.Queue(maxsize=self._queuesize)
        self.buffer = []
        self._serialport = serialport
        self._timeout = timeout
        self._filename = filename
        self._linesReadSinceCSVUpdate = 0
        self._loggingActive = False
        self._threadRunning = True # have to assume it is true...

    def _queueDequeuer(self):
        while True:
            try:
                yield self._q.get_nowait()
            except queue.Empty:
                break

    def _parseSerialData(self, serialDataString):
        ### parses the serial data
        datadict = {}
        try:
            if "timestamp" in str(serialDataString):
                datadict = json.loads(serialDataString)
            return datadict
        except:
            return datadict #return empt datadict if anything goes wront

    def _updateBuffer(self, datadict):
        ### Returns: list corresponding to the current queue

        # Add it to the queue
        if self._q.full():
            self._q.get(False)
            self._q.put(datadict, False)
        else:
            self._q.put(datadict, False)

        # Update the list (buffer) with the latest contents of the queue
        x = [item for item in self._queueDequeuer()]
        for xi in x:
            if "timestamp" in xi: # Only append records which have a timestamp associated with them
                self.buffer.append(xi)
                self._linesReadSinceCSVUpdate += 1

    def _updateCSV(self):
        with open(self._filename, 'a') as dtf:
            print("Writing {0} lines to CSV".format(len(self.buffer)))

            for line in self.buffer:
                linestr = str(line.values())
                linestr_trimmed = linestr[13:len(linestr)-2] #'dict_values([val1, val2])'
                dtf.write(linestr_trimmed)
                dtf.write("\n") #explicitly write newline
                #dtf.write(str(line.values())[1:len(line)])
            self.buffer = []
            self._linesReadSinceCSVUpdate = 0

    def processSerialData(self):
        rawdata = self._serialport.readline(self._timeout)
        parsedData = self._parseSerialData(rawdata)
        self._updateBuffer(parsedData)
        if self._linesReadSinceCSVUpdate % self._queuesize == 0 and self._linesReadSinceCSVUpdate > 0:
            self._updateCSV()

    def run(self):
        super(SerialDataMonitorThread, self).__init__()
        print("Started Thread")
        while (self._loggingActive):
            self.processSerialData()
        self._updateCSV()  # write the data that hasn't been written to the CSV yet
        print("Thread ended.")
        self._threadRunning = False
        return

    def startLogging(self):
        print("Called startLogging()")
        self._loggingActive = True

    def stopLogging(self):
        print("Called stopLogging()")
        self._loggingActive = False




#garbage nonworking sample code for pyqt - plot freezes
import numpy as np
#x = np.random.normal(size=1000)
#y = np.random.normal(size=1000)
#pg.plot(x, y, pen=None, symbol='o')


#matplotlib way

#select style to pretty up graph
style.use('fivethirtyeight')

#creates new window for plots
fig = plt.figure()

ax1 = fig.add_subplot(1,1,1)


#function stolen from youtube python wizard to plot a file that is updating
def animate(data):
    global fig, ax1

    xs=[]
    ys=[]
    for d in data:
        if 'timestamp' in d.keys():
            xs.append(d['timestamp'])
            ys.append(d['timestamp2'])
    ax1.clear()
    ax1.plot(xs,ys, '-o')
    #plt.axes().autoscale()
    plt.ticklabel_format(style='sci', axis='x', scilimits=(0, 0))
    #plt.show()


baudrate = 115200

print('Begin program - find available serial ports.')
#connect2arduino()
ports_avail = stools_ports.comports()
print('Port scan complete.')

for port in ports_avail:
    print(str(port))

print('Using port 4 (kyle\'s MAC) because reasons.')
ser= serial.Serial(ports_avail[4].device,baudrate= baudrate)

#
serialDataThread = SerialDataMonitorThread(300, ser, filename='datafile.csv')
serialDataThread.startLogging() #get ready to logthedatas

serialDataThread.start() # Start the thread. Multithreaded

# win = pg.GraphicsWindow()
# win.setWindowTitle('pyqtgraph example: Scrolling Plots')
#
# # 3) Plot in chunks, adding one new plot curve for every 100 samples
# chunkSize = 100
# # Remove chunks after we have 10
# maxChunks = 10
# startTime = pg.ptime.time()
# p5 = win.addPlot()
# p5.setLabel('bottom', 'Time', 's')
# p5.setXRange(-10, 0)
# curves = []
# data5 = np.empty((chunkSize + 1, 2))
# ptr5 = 0
#
# def update3(data):
#     global p5, data5, ptr5, curves
#     now = pg.ptime.time()
#     for c in curves:
#         c.setPos(-(now - startTime), 0)
#
#     i = ptr5 % chunkSize
#     if i == 0:
#         curve = p5.plot()
#         curves.append(curve)
#         last = data5[-1]
#         data5 = np.empty((chunkSize + 1, 2))
#         data5[0] = last
#         while len(curves) > maxChunks:
#             c = curves.pop(0)
#             p5.removeItem(c)
#     else:
#         curve = curves[-1]
#     data5[i + 1, 0] = now - startTime
#     data5[i + 1, 1] = np.asarray(data)
#     curve.setData(x=data5[:i + 2, 0], y=data5[:i + 2, 1])
#     ptr5 += 1
#
# def update():
#     try:
#         print ("Main Loop")
#         for buffItem in serialDataThread.buffer:
#             update3(buffItem["timestamp"])
#
#         #serialDataThread.join(timeout=1000) #don't know if you need this timeout thing
#     except Exception as e:
#         print(e)
#         serialDataThread.stopLogging()
#         #serialDataThread.join()


# timer = pg.QtCore.QTimer()
# timer.timeout.connect(update)
# timer.start(50)
#
# if (sys.flags.interactive != 1) or not hasattr(QtCore, 'PYQT_VERSION'):
#         QtGui.QApplication.instance().exec_()

# ani = animation.FuncAnimation(fig, animate, interval=1000)
# animate(serialDataThread.buffer)
# plt.show()

while serialDataThread._threadRunning:
    try:
        time.sleep(1)

            # serialDataThread.join(timeout=1000) #don't know if you need this timeout thing
    except KeyboardInterrupt:
        serialDataThread.stopLogging()
        # serialDataThread.join()




#serialDataThread.run() #NOT MULTITHREADED

#serialDataThread.join #this would be used to wait until the thread completes

# while True:
#     try:

    # here we should be able to access the thread's data, but we might need to wrap some semaphores around it or use multiprocessing


    # #if the line contains (or starts with? the timestamp, add it to datastrings for post processing
    # if str(temp).__contains__("timestamp"):
    #
    #     #adding the datas
    #     datastrings.append(temp)
    #
    #     #this parses the json data into a dictionary
    #     datadict=json.loads(temp)
    #     datadictvaluelist=str(list(datadict.values()))
    #
    #     datafilecsv.write(datadictvaluelist[1:(len(datadictvaluelist)-1)]+'\n')
    #     #pg.plot(datadictvaluelist)  # data can be a list of values or a numpy array
    #
    #     lines=datafilecsv.readlines()
    #     linesindatafile=len(lines)
    #
    #     #handles writing buffer file while waiting for first 50 entries
    #     #not sure this first if is necessary
    #     if len(lines)>1:
    #         if linesindatafile<buffersize:
    #             buffercsv = open('buffer.csv', 'w')
    #             buffercsv.write(str(lines))
    #             buffercsv.close()
    #
    #         #once 50 entries exist in file, copy latest 50
    #         else:
    #             buffercsv = open('buffer.csv', 'w')
    #             buffercsv.truncate
    #             lastNlines=lines[(linesindatafile-(buffersize+1)):(linesindatafile-1)]
    #             for l in lastNlines:
    #                 l=str(l).replace('[','').replace(']','')
    #                 buffercsv.write(l)
    #             buffercsv.close()


# datafilecsv.close()


#animate is blocking any code past plt.show() from running until the plot window is closed
# do we need a different script running the serial ports stuff? seems like it would work better


# for i in range(1,100):
#     temp = ser.readline(300)
#     serialtocsv(temp)
#     if divmod(i,10)[1]==0:
#         print(i/10)
# ser.close()

#poo.plot(list(datadict.values()))

#hi